package fr.devid.app.data

import java.util.*

data class Cle(
    val id: UUID,
    val nom: String,
    val adresse: String,
    val arrondissement: String,
    val color: Int
)

val listKeys = listOf<Cle>(Cle(UUID.randomUUID(), "Brandy Norwood", "4 rue Louis Grobet", "13001 Marseille", 1),
    Cle(UUID.randomUUID(), "Pierre Richard", "14 rue des Ivrognes", "13666 Marseille", 2),
    Cle(UUID.randomUUID(), "James Franco", "5 rue Paradis", "13006", 3))
