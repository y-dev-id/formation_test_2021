package fr.devid.app.ui.keys

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.devid.app.ui.keys.recyclerview.KeyUser
import javax.inject.Inject

// VIEW MODEL CREATED IN CASE WE DECIDE TO HANDLE USERS DATA WITH THE RIGHT MANNER

@HiltViewModel
class KeysViewModel @Inject constructor() : ViewModel() {

    private val _usersLiveData = MutableLiveData<List<KeyUser>>(null)
    val userLiveData: LiveData<List<KeyUser>>
        get() = _usersLiveData

    fun getUsers() {
        // Put your code here in order to get keyUsers
    }
}
