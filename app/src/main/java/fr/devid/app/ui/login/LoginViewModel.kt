package fr.devid.app.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.devid.app.services.SharedPreferencesService
import fr.devid.app.ui.login.LoginFragment.Companion.EMAIL_TO_CHECK
import fr.devid.app.ui.login.LoginFragment.Companion.PASSWORD_TO_CHECK
import fr.devid.app.viewmodels.Event
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val sharedPreferencesService: SharedPreferencesService
) : ViewModel() {

    enum class AuthenticationState {
        AUTHENTICATED,
        UNAUTHENTICATED
    }

    enum class LoginState {
        FILL_FIELDS,
        NO_INTERNET,
        WRONG_CREDENTIALS,
        NOT_ACTIVATED
    }

    val authenticationState: LiveData<AuthenticationState>
        get() = _authenticationState
    val loginState: LiveData<Event<LoginState>>
        get() = _loginState

    private val _isLoading = MutableLiveData(false)
    val _authenticationState = MutableLiveData<AuthenticationState>()
    private val _loginState = MutableLiveData<Event<LoginState>>()

    init {
        _authenticationState.value =
            if (sharedPreferencesService.token == null) AuthenticationState.UNAUTHENTICATED else AuthenticationState.AUTHENTICATED
    }

    fun login(email: String?, password: String?) {
        if (email.isNullOrEmpty() || password.isNullOrEmpty()) {
            _loginState.value = Event(LoginState.FILL_FIELDS)
        } else if (email == EMAIL_TO_CHECK && password == PASSWORD_TO_CHECK) {
            sharedPreferencesService.token = "token"
            _authenticationState.value = AuthenticationState.AUTHENTICATED
        } else {
            sharedPreferencesService.token = null
            _authenticationState.value = AuthenticationState.UNAUTHENTICATED
            _loginState.value = Event(LoginState.WRONG_CREDENTIALS)
        }
    }

    fun logout() {
        sharedPreferencesService.token = null
        _authenticationState.value = AuthenticationState.UNAUTHENTICATED
    }
}
