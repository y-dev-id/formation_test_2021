package fr.devid.app.ui.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.devid.app.R
import fr.devid.app.ui.login.LoginViewModel
import fr.devid.app.services.SharedPreferencesService
import fr.devid.app.viewmodels.Event
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val sharedPreferencesService: SharedPreferencesService
) : ViewModel() {

    enum class RegisterViewModelState {
        IDLE, LOADING, SUCCESS
    }

    val state: LiveData<RegisterViewModelState>
        get() = _state

    private val _state = MutableLiveData(RegisterViewModelState.IDLE)
    private val errorMessageResourceEvent = MutableLiveData<Event<Int>>()

    fun register(email: String, password: String) {
        if (email.isNotEmpty() && password.isNotEmpty()) {
            _state.value = RegisterViewModelState.SUCCESS
        } else {
            errorMessageResourceEvent.value = Event(R.string.fill_all_fields)
        }
    }
}
