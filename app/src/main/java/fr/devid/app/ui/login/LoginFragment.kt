package fr.devid.app.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import fr.devid.app.BuildConfig
import fr.devid.app.R
import fr.devid.app.base.BaseFragment
import fr.devid.app.databinding.FragmentLoginBinding
import fr.devid.app.viewmodels.EventObserver

class LoginFragment : BaseFragment() {

    private val loginViewModel: LoginViewModel by activityViewModels()

    companion object {
        const val EMAIL_TO_CHECK = "bob@test.com"
        const val PASSWORD_TO_CHECK = "Aa1234567!"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentLoginBinding.inflate(inflater, container, false)

        val navController = findNavController()
        bindUi(binding, navController)
        subscribeUi(navController)

        return binding.root
    }

    private fun bindUi(binding: FragmentLoginBinding, navController: NavController) {
        if (BuildConfig.DEBUG) {
            binding.etLoginEmail.setText("bob@test.com")
            binding.etLoginPassword.setText("Aa1234567!")
        }
        binding.btLogin.setOnClickListener {
                loginViewModel.login(
                    binding.etLoginEmail.text?.toString(),
                    binding.etLoginPassword.text?.toString()
                )
        }
        binding.tvToRegister.setOnClickListener {
            navController.navigate(
                LoginFragmentDirections.actionLoginFragmentToRegisterFragment()
            )
        }
        binding.tvForgottenPassword.setOnClickListener {
        }
    }

    private fun subscribeUi(navController: NavController) {
        loginViewModel.authenticationState.observe(viewLifecycleOwner, {
            if (it == LoginViewModel.AuthenticationState.AUTHENTICATED) {
                navController.navigate(LoginFragmentDirections.actionLoginFragmentToMainFragment())
            }
        })

        loginViewModel.loginState.observe(viewLifecycleOwner, EventObserver {
            val errorResource = when (it) {
                LoginViewModel.LoginState.FILL_FIELDS -> R.string.fill_all_fields
                LoginViewModel.LoginState.NO_INTERNET -> R.string.check_internet
                LoginViewModel.LoginState.NOT_ACTIVATED -> R.string.not_activated
                LoginViewModel.LoginState.WRONG_CREDENTIALS -> R.string.wrong_credentials
            }
            Toast.makeText(context, errorResource, Toast.LENGTH_LONG).show()
        })
    }
}
