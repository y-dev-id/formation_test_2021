package fr.devid.app.ui.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import fr.devid.app.base.BaseFragment
import fr.devid.app.databinding.FragmentRegisterBinding
import fr.devid.app.ui.login.LoginViewModel

class RegisterFragment : BaseFragment() {

    private val registerViewModel: RegisterViewModel by viewModels()
    private val loginViewModel: LoginViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentRegisterBinding.inflate(inflater, container, false)

        val navController = findNavController()
        bindUi(binding, navController)
        subscribeUi(navController)

        return binding.root
    }

    private fun bindUi(binding: FragmentRegisterBinding, navController: NavController) {
        binding.btnRegister.setOnClickListener {
            registerViewModel.register(binding.etRegisterEmail.text.toString(),
            binding.etRegisterPassword.text.toString()
            )
        }
        binding.tvToLogin.setOnClickListener {
            navController.navigate(RegisterFragmentDirections.actionRegisterFragmentToLoginFragment2())
        }
    }

    private fun subscribeUi(navController: NavController) {
        registerViewModel.state.observe(viewLifecycleOwner, {
            if (it == RegisterViewModel.RegisterViewModelState.SUCCESS) {
                loginViewModel._authenticationState.value = LoginViewModel.AuthenticationState.AUTHENTICATED
                navController.navigate(RegisterFragmentDirections.actionRegisterFragmentToMainFragment())
            }
        })
    }
}
