package fr.devid.app.ui.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import fr.devid.app.R
import fr.devid.app.data.Cle
import fr.devid.app.databinding.RvItemKeysBinding

class ListKeysAdapter : ListAdapter<Cle, ListKeysAdapter.MyViewHolder>(MyDiffUtil()) {

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = RvItemKeysBinding.bind(itemView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.rv_item_keys, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val key: Cle = getItem(position)
/*
        val backgroundColor = when (getItem(position).color) {
            1 -> Color.parseColor("#FF9800")//Orange
            //2 -> Color.parseColor("F8BC34A")//Vert
            else -> Color.parseColor("FF44336") //Rouge
        }
        holder.binding.rvItemKey.setBackgroundColor(backgroundColor)

 */
        with(holder.binding) {
            tvNom.text = key.nom
            tvAdresse.text = key.adresse
            tvArrondissement.text = key.arrondissement
        }
    }
}

class MyDiffUtil : DiffUtil.ItemCallback<Cle>() {
    override fun areItemsTheSame(oldItem: Cle, newItem: Cle) = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Cle, newItem: Cle) = oldItem == newItem

}
