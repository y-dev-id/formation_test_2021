package fr.devid.app.ui.keys

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.devid.app.R
import fr.devid.app.databinding.FragmentKeysBinding
import fr.devid.app.ui.keys.recyclerview.KeyUser
import fr.devid.app.ui.keys.recyclerview.UserAdapter

class KeysFragment : Fragment() {

    private lateinit var adapter: UserAdapter
    private var _binding: FragmentKeysBinding? = null
    private val binding
        get() = _binding!!

    private val viewModelKeys: KeysViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentKeysBinding.inflate(layoutInflater, container, false)
        adapter = UserAdapter()
        val rv: RecyclerView = binding.rvKeyUsers
        rv.layoutManager = GridLayoutManager(this.context, 2)

        rv.adapter = adapter

        adapter.onItemCallBack = {

            binding.tvCvName.text = it.name
            binding.tvCvAdress.text = it.address
            binding.tvCvKeyNumber.text = it.keyUser

            // Handle card colors
            when (it.keyStatus) {
                1 -> {
                    binding.cvDisplay.setBackgroundResource(R.color.green)
                }
                2 -> {
                    binding.cvDisplay.setBackgroundResource(R.color.orange)
                }
                else -> {
                    binding.cvDisplay.setBackgroundResource(R.color.red)
                }
            }
        }

        // DEMO LIST OF USERS
        val list = listOf(
            KeyUser(1, "Darren Fletcher", "Avenue de la grotte", "A452766", true, 1),
            KeyUser(2, "Yarno Truli", "Rue de la paix", "A452765", true, 2),
            KeyUser(3, "Steeve Mc Manaman", "Avenue de la rue", "A452764", true, 3),
            KeyUser(4, "Trey Azagtoth", "Rue de la brume", "A452763", true, 1),
            KeyUser(5, "Igor Cavalera", "Rue de la peisca", "A452762", false, 2)
        )

        binding.ivRefresh.setOnClickListener {
            // DEMO REFRESH
            viewModelKeys.getUsers()
        }

        adapter.submitList(list)

        return binding.root
    }
}
