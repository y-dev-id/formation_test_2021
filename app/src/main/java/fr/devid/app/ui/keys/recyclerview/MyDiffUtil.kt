package fr.devid.app.ui.keys.recyclerview

import androidx.recyclerview.widget.DiffUtil

// Works only if it's DATACLASS

class MyDiffUtil : DiffUtil.ItemCallback<KeyUser>() {
    override fun areItemsTheSame(oldItem: KeyUser, newItem: KeyUser): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: KeyUser, newItem: KeyUser): Boolean {
// Comparer les variables d'instance
        return oldItem == newItem
    }
}
