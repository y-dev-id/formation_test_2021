package fr.devid.app.ui.keys.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import fr.devid.app.R
import fr.devid.app.databinding.ItemCardUserBinding

class UserAdapter : ListAdapter<KeyUser, UserAdapter.UserHolder>(MyDiffUtil()) {
    var onItemCallBack: ((KeyUser) -> Unit)? = null

    inner class UserHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = ItemCardUserBinding.bind(itemView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_card_user, parent, false)
        return UserHolder(view)
    }

    override fun onBindViewHolder(holder: UserHolder, position: Int) {

        val keyUser: KeyUser = getItem(position)

        with(holder.binding) {
            tvItemUserName.text = keyUser.name
            tvItemAddress.text = keyUser.address
            tvItemNumKey.text = keyUser.keyUser
        }

        // BorderRadius disabled when we set colors to User CardView -> Investigations in progress

        val color = when (keyUser.keyStatus) {
            1 -> R.color.green
            2 -> R.color.orange
            else -> R.color.red
        }

        holder.binding.cvItemUser.setCardBackgroundColor(ContextCompat.getColor(holder.itemView.context, color))

        holder.binding.cvItemUser.setOnClickListener {
            onItemCallBack?.invoke(keyUser)
        }
    }
}
