package fr.devid.app.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import fr.devid.app.R
import fr.devid.app.base.BaseFragment
import fr.devid.app.data.listKeys
import fr.devid.app.databinding.FragmentHomeBinding
import fr.devid.app.ui.home.adapter.ListKeysAdapter

class HomeFragment : BaseFragment() {
    //VARIABLES D'INSTANCES
    var latitude = 0f
    var longitude = 0f

    //BINDING
    private var _binding: FragmentHomeBinding? = null
    private val binding
        get() = _binding!!

    //BINDING
    lateinit var adapter: ListKeysAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //GESTION DE LA MAP
        val apiKey = getString(R.string.google_api_key)

        //GESTION DU RECYCLER
        binding.recyclerView
        adapter = ListKeysAdapter()
        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)

        adapter.submitList(listKeys)
    }

}
