package fr.devid.app.ui.keys.recyclerview

data class KeyUser(
    val id: Long,
    val name: String,
    val address: String,
    val keyUser: String,
    val isKeyAllowed: Boolean,
    val keyStatus: Int,
)
