package fr.devid.app.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.devid.app.api.ProfileDto
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor() : ViewModel() {

    val profile: LiveData<ProfileDto>
        get() = _profile

    private val _profile = MutableLiveData<ProfileDto>()
}
