package fr.devid.app.ui.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import fr.devid.app.base.BaseFragment
import fr.devid.app.services.SharedPreferencesService
import javax.inject.Inject
import fr.devid.app.databinding.FragmentSplashBinding

@AndroidEntryPoint
class SplashFragment : BaseFragment() {

    @Inject
    internal lateinit var sharedPreferencesService: SharedPreferencesService
    private var _binding: FragmentSplashBinding? = null
    private val binding
        get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val binding = FragmentSplashBinding.inflate(layoutInflater, container, false)

        val navController = findNavController()

        binding.btnDemarrer.setOnClickListener {

            navController.navigate(SplashFragmentDirections.actionSplashFragmentToLoginGraph())
        }

        return (binding.root)
    }

}


/*private fun subscribeUi(navController: NavController) {
loginViewModel.authenticationState.observe(viewLifecycleOwner, {
 */
/* when (it) {
               LoginViewModel.AuthenticationState.AUTHENTICATED -> navController.navigate(
                   SplashFragmentDirections.actionSplashFragmentToMainFragment()
               )
               LoginViewModel.AuthenticationState.UNAUTHENTICATED -> navController.navigate(
                   SplashFragmentDirections.actionSplashFragmentToLoginGraph()
               )
               else -> {
               } // Nothing to do
               }

           }
         }) */

